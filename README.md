How to setup this XCompose configuration
========================================

Requirements
------------

You need a relatively recent version of [GHC](https://www.haskell.org/ghc/),
the Glasgow Haskell Compiler.


Installation steps
------------------

Delegate a key to serve as your "Compose Key". This can be system-dependent.
In Debian and derivatives (Ubuntu, etc.), add the following line to `/etc/default/keyboard`:

```
XKBOPTIONS="compose:caps"
```

This will setup the Caps Lock key as the system's "Compose Key".

Optionally you can have a look and modify the mappings in `xcompose-generator/XComposeGen/*.hs`,
add new symbols or change shortcuts.

Then, generate the `.XCompose` file by running `./install.sh`.
This script automatically symlinks the generated `XCompose` file to the standard location of `$HOME/.XCompose`.
The location in which the generated file will be place (and thus the location to which the symlink points to)
can be chosen by passing the first parameter to the script.

Moreover if you want it to be run automatically you could have a look at [mr](http://joeyh.name/code/mr/)
and setup a fixups hook:

```
    [.XCompose.d]
    checkout = git clone git@github.com:np/xcompose.git .XCompose.d
    fixups = ./install.sh
```

